---
theme : "night"
transition: "convex"
highlightTheme: "monokai"
logoImg: "Own_backup_white.png"
slideNumber: false
title: "Alembic workshop"
---

<script>
Reveal.initialize({
  dependencies: [
    { src: '//cdn.jsdelivr.net/npm/reveal-plantuml' },
  ]
});
  </script>
<style>
.left {
  left:-8.33%;
  text-align: left;
  float: left;
  width:50%;
  z-index:-10;
}

.right {
  left:31.25%;
  top: 75px;
  float: right;
  text-align: right;
  z-index:-10;
  width:50%;
}


.left-25 {
  left:-8.33%;
  text-align: left;
  float: left;
  width:25%;
  z-index:-10;
}

.right-25 {
  left:31.25%;
  top: 75px;
  float: right;
  text-align: right;
  z-index:-10;
  width:25%;
}

.left-75 {
  left:-8.33%;
  text-align: left;
  float: left;
  width:75%;
  z-index:-10;
}

.right-75 {
  left:31.25%;
  top: 75px;
  float: right;
  text-align: right;
  z-index:-10;
  width:75%;
}


.left-33 {
  left:-8.33%;
  text-align: left;
  float: left;
  width:33%;
  z-index:-10;
}

.right-33 {
  left:31.25%;
  top: 75px;
  float: right;
  text-align: right;
  z-index:-10;
  width:33%;
}

.left-66 {
  left:-8.33%;
  text-align: left;
  float: left;
  width:66%;
  z-index:-10;
}

.right-66 {
  left:31.25%;
  top: 75px;
  float: right;
  text-align: right;
  z-index:-10;
  width:66%;
}

</style>

# Alembic workshop


*your guide how to sleep well at night when your code gone to production*

<aside class="notes">test test test</aside>

---

## What is alembic?

Alembic is a tool that manages service migrations.

Keeping database scheme healthy ensuring that new changes would not break old database.

You can read more at: [https://alembic.sqlalchemy.org/en/latest/](https://alembic.sqlalchemy.org/en/latest/)

<aside class="notes"></aside>

---


#### Prerequesites to start the workshop

1. clone project to your local machine:
    ```shell
    git clone https://gitlab.com/vladimirp2/alembic-workshop
2. Go to directory of alembic-workshop
3. install poetry (if you don't have it yet)

```shell
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -;
```
4. install dependencies

  ```shell
  poetry install
  poetry shell
  ```






---


# Exercise 1
--- 
Create database and start service

```shell
git fetch --all;git clean -fdx;git stash;git stash drop;
git checkout  first-exercise
poetry shell
export PYTHONPATH=$PWD/alembic_workshop
poetry run service
```

--

## How to initialize alembic?

At root of your project run:

```shell
poetry add alembic
alembic init alembic
```

Where the second alembic is the name of the directory where you want to initialize alembic.



--

## Configure `alembic.ini`

As alembic is a tool that relies on specific database ofcourse we need to provide a connection string.


<pre><code data-line-numbers="54">
# A generic, single database configuration.

[alembic]
# path to migration scripts
script_location = alembic

# template used to generate migration files
# file_template = %%(rev)s_%%(slug)s

# sys.path path, will be prepended to sys.path if present.
# defaults to the current working directory.
prepend_sys_path = .

# timezone to use when rendering the date within the migration file
# as well as the filename.
# If specified, requires the python-dateutil library that can be
# installed by adding `alembic[tz]` to the pip requirements
# string value is passed to dateutil.tz.gettz()
# leave blank for localtime
# timezone =

# max length of characters to apply to the
# "slug" field
# truncate_slug_length = 40

# set to 'true' to run the environment during
# the 'revision' command, regardless of autogenerate
# revision_environment = false

# set to 'true' to allow .pyc and .pyo files without
# a source .py file to be detected as revisions in the
# versions/ directory
# sourceless = false

# version location specification; This defaults
# to alembic/versions.  When using multiple version
# directories, initial revisions must be specified with --version-path.
# The path separator used here should be the separator specified by "version_path_separator"
# version_locations = %(here)s/bar:%(here)s/bat:alembic/versions

# version path separator; As mentioned above, this is the character used to split
# version_locations. Valid values are:
#
# version_path_separator = :
# version_path_separator = ;
# version_path_separator = space
version_path_separator = os  # default: use os.pathsep

# the output encoding used when revision files
# are written from script.py.mako
# output_encoding = utf-8

sqlalchemy.url = sqlite:///alembic_workshop/service.db


[post_write_hooks]
# post_write_hooks defines scripts or Python functions that are run
# on newly generated revision scripts.  See the documentation for further
# detail and examples 

# format using "black" - use the console_scripts runner, against the "black" entrypoint
# hooks = black
# black.type = console_scripts
# black.entrypoint = black
# black.options = -l 79 REVISION_SCRIPT_FILENAME

# Logging configuration
[loggers]
keys = root,sqlalchemy,alembic

[handlers]
keys = console
</code></pre>

--

## Configure `alembic/env.py`

The second part of our process is import all our models and add base object containing sqlalchemy metadata.

<pre><code data-line-numbers="6-7|22">
from logging.config import fileConfig

from sqlalchemy import engine_from_config
from sqlalchemy import pool
from alembic_workshop.models import *
from alembic_workshop.models import Base
from alembic import context

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = Base.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
</code></pre>

--

## One more time

Now try:
```shell
poetry run service
```

---

# Exercise 2
--- 
Create revision and upgrade
```shell
git clean -fdx;git stash;git stash drop; 
git checkout second-exercise;
poetry shell
export PYTHONPATH=$PWD/alembic_workshop
poetry run service
```


--

## How to create revision (migration)?

```shell
alembic revision --autogenerate -m "Initial migration"
```
`--autogenerate` - will try to generate schema changes automatically. And usually it works well.

But always check if you have all the changes you need.

--

## What changed?

go to `alembic/versions` and check what was created.

<pre><code data-line-numbers="16-17|22-29|33-35">


"""Initial migration

Revision ID: cbd95acc1710
Revises: 
Create Date: 2022-01-26 09:54:59.934651

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cbd95acc1710'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('messages',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('message', sa.String(), nullable=True),
    sa.Column('created', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('messages')
    # ### end Alembic commands ###



</code></pre>

--

  ## Try to run service once more

  ```shell
  poetry run service
  ```

--

## How to upgrade database?

```shell
alembic upgrade head
```



---


# Exercise 3
--- 
Merge multiple heads

```shell
  git clean -fdx;git stash;git stash drop; 
  git checkout  third-exercise
  alembic upgrade head
```

--


## Linked list

#### When you surely don't have any problems with database?

When your migrations graph is linear

```plantuml
@startuml
!theme spacelab
(A) -> (B)
(B)->(C)
@enduml
```

In this case `alembic upgrade head` will work for sure

--

## Tree graph

#### But what if you want several developers working on different database features simultaneously?

```plantuml
@startuml
!theme spacelab
(A) -> (B)
(B)->(C)
(C)--->(D)
(C)--->(E)
(C)--->(F)
@enduml
```

--

## Useful commands

### How do we know current status?

`alembic heads` - will yield current heads (if you have only one - you are ok)

--- 

`alembic branches` - will yield all branches that were existing or existing now


--

## How to look at history

### How do we know current status?

`alembic history` - if you want to see the whole graph

```shell
b7aa96ebb2ca -> 133f597af13c (head), added timestamp
b7aa96ebb2ca -> 8a69621401b2 (head), added topic
b7aa96ebb2ca -> 743035f4c1ce (head), added urgency
<base> -> b7aa96ebb2ca (branchpoint), Initial migration
```


--


## Merge heads: Before

#### So how can we merge things right?


<div class="left-66">


```shell
alembic history
C -> F, some message
C -> E, some message
C -> D, some message
B -> C (branchpoint), some message
A -> B, some message
<base> -> A, some message

```

```shell
alembic heads
D (head)
E (head)
F (head)
```



</div>

<div class="right-33">

```plantuml
@startuml
!theme spacelab
(A) -> (B)
(B)->(C)
(C)--->(D)
(C)--->(E)
(C)--->(F)
@enduml
```
</div>



--

## Merge heads: First merge

#### So how can we merge things right?


<div class="left-66">


```shell
alembic merge D E -m "merge D and E"
```

```shell
alembic history
D, E -> G (head) (mergepoint), merge D and E
C -> F, some message
C -> E, some message
C -> D, some message
B -> C (branchpoint), some message
A -> B, some message
<base> -> A, some message

```

```shell
alembic heads
G (head)
F (head)
```



</div>

<div class="right-33">

```plantuml
@startuml
!theme spacelab
(A) -> (B)
(B)->(C)
(C)--->(D)
(C)--->(E)
(C)--->(F)
(E)--->(G)
(D)--->(G)
@enduml
```
</div>


--

#### Merge heads: Second merge

##### So how can we merge things right?


<div class="left-66">

```shell
alembic merge G F -m "merge G and F"
```

```shell
alembic history
G, F -> H (head) (mergepoint), merge G and F
D, E -> G, merge D and E
C -> F, some message
C -> E, some message
C -> D, some message
B -> C (branchpoint), some message
A -> B, some message
<base> -> A, some message

```

```shell
alembic heads
H (head)
```


run *`alembic upgrade head`* 

and you done!




</div>

<div class="right-33">

```plantuml
@startuml
!theme spacelab
(A) -> (B)
(B)->(C)
(C)--->(D)
(C)--->(E)
(C)--->(F)
(E)--->(G)
(D)--->(G)
(G)--->(H)
(F)--->(H)
@enduml
```
</div>

--

## Merge heads: Automatically

Also you may try to solve it automatically - usually it helps

```shell
alembic merge heads
```

But sometimes it fails...So...

--

# KEEP ONE HEAD AT MAIN BRANCH

---

# Exercise 4
--- 
Run python on top of migration

```shell
git clean -fdx;git stash;git stash drop; 
git checkout  fourth-exercise
poetry shell
export PYTHONPATH=$PWD/alembic_workshop
poetry run service

curl -X 'GET' \
  'http://127.0.0.1:8000/messages' \
  -H 'accept: application/json'
```

--

## Running our python code on top of migration

In general alembic not supposed to be used as data migration tool, but...

--

#### Running our python code on top of migration

we can use sqlalchemy to run our code on top of migration.


<pre><code data-line-numbers="9-10|21|28-29|37-41">
"""retroactive timestamps

Revision ID: 6cde65409b3a
Revises: 415fba8c4a91
Create Date: 2022-01-25 18:08:29.417509

"""
from alembic import op
import sqlalchemy as sa
import debugpy
from alembic_workshop.models import Message

# revision identifiers, used by Alembic.
revision = "6cde65409b3a"
down_revision = "415fba8c4a91"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    connection = op.get_bind()
    print("waiting for debugger at localhost:5678")
    debugpy.listen(5678)
    debugpy.wait_for_client()

    messages_table = Message.__table__
    messages = connection.execute(sa.select(messages_table)).all()

    def get_timestamp(message):
        return int(message["created"].timestamp())

    for message in map(lambda m: dict(m), messages):
        if message["timestamp"] is None:
            timestamp = get_timestamp(message)
            connection.execute(
                messages_table.update()
                .where(messages_table.c.id == message["id"])
                .values(timestamp=timestamp)
            )


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


</code></pre>


--

## Code migration result

```shell
git clean -fdx;git stash;git stash drop; 
git checkout  fourth-exercise-result
poetry shell
export PYTHONPATH=$PWD/alembic_workshop
poetry run service

curl -X 'GET' \
  'http://127.0.0.1:8000/messages' \
  -H 'accept: application/json'
```


---


## Migration status

### Is there a way to know that migration is successful?

Unfortunately, there is no 100% way to know that migration is successful.

But here what we can do:

--

## Migration status

- ensure that you have database backup before applied migrations

- successful `alembic upgrade head` - means that changes of the schema went as planned

- keep up to date your ***unit tests*** and run them on:
  - new database made from scratch
  - some old database which was somehow used before that migration

--

> **WARNING**: Always verify that your migrations do not drop columns or tables unless it was your 110% intention.


---

# Appendix
--- 
How to run migrations tests on platform?

--

## Pytest level

`--connection` - you can use real database as test database

example: `pytest --connection sqlite:///yourdb.sqlite`

--- 

`--shell-before-all` - you can run shell commands before all tests.

example: `pytest --shell-before-all "echo 'hello world'"`

--- 

`--shell-after-all` - you can run shell commands after all tests.

example: `pytest --shell-after-all "echo 'goodbye world'"`

--

## Poetry level

Poetry level scripts made to automate backup and recovery process.
They can both run both in local machine and on ci pipeline.

--

## Poetry level scripts

### `poetry run migrations-test` - run migrations tests

`--connection` - you can use real database as test database

`--seed-sql` - you can use sql seed file as your base for database

`--successfull-stay` - do not rollback database after tests if they are successful

`--finally-clean` - clean all generated things after tests - useful on local machine

`--generate-postgres` - generate postgresql database for tests

--

## Owns level


will generate `.owns` file under service directory - it's private for each user so don't push it to git

And will trigger all underlying layers to run migrations test

by command `owns migrations test`

And generate `.owns` file by running `owns migrations init`

---

### After all horrors we've seen: Why not to use SqlAlchemy create_all?

- It can wipe things when we want to add additional columns to existing table.
- Sometimes we do want to generate new columns and fill them with values before the runtime.
- Cases when we want to create database with predefined users before runtime.

Bottom line: It will surely corrupt your database. Use alembic!


---

# THE END