import uvicorn
from fastapi import FastAPI, Depends
from fastapi.responses import JSONResponse, RedirectResponse
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Session


engine = create_engine(
    "sqlite:///service.db", connect_args={"check_same_thread": False}
)


Base = declarative_base()


class Message(Base):
    __tablename__ = "messages"

    id = Column(Integer, primary_key=True)
    message = Column(String)
    created = Column(DateTime)


Base.metadata.create_all(engine)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


app = FastAPI()


def get_session():
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()


@app.get("/")
async def root():
    return RedirectResponse(url="/docs")


@app.get("/messages")
async def get_messages(session: Session = Depends(get_session)):
    return [
        {
            "message": message.message,
            "created": message.created.isoformat(),
            "id": message.id,
        }
        for message in session.query(Message).all()
    ]


@app.delete("/message")
async def delete_message(id: int, session: Session = Depends(get_session)):
    session.query(Message).filter(Message.id == id).delete()
    session.commit()
    return {"message": "Message deleted"}


@app.post("/message")
async def create_message(message: str, session: Session = Depends(get_session)):
    new_message = Message(message=message, created=datetime.now())
    session.add(new_message)
    session.commit()
    return {"message": "Message created"}


def service():
    uvicorn.run("main:app", port=8000, reload=True)
